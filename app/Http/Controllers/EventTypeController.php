<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Model\EventType;

class EventTypeController extends Controller {
    public function __construct() { }

    public function get(Request $request) {
        $body = [];
        $code = 200;
        try {
            $body = EventType::select('id', 'title', 'duration')->where('user_id', $request->user_id)->get();
            return response()->json($body, $code);
        } catch(\Exception $e) {
            return response()->json($body, $code);
        }
    }

    public function add(Request $request) {
        $body = [
            'message' => 'Validation Failed'
        ];
        $code = 400;
        $rules = [
            'title' => 'required|max:50',
            'duration' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($body, $code);
        }
        $types = new EventType();
        $types->user_id = $request->user_id;
        $types->title = $request->title;
        $types->duration = $request->duration;
        
        try {
            $data = $types->save();
            $code = 201;
            $body = $types;
        } catch (\Exception $e) {
            $body = [
                'message' => 'Something went wrong'
            ];
        }
        return response()->json($body, $code);
    }

    public function update(Request $request, $id) {
        $body = [
            'message' => 'Validation Failed'
        ];
        $code = 400;
        $rules = [
            'title' => 'max:50',
            'duration' => 'integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($body, $code);
        }

        $type = EventType::find($id);
        if (!$type) {
            $body = [
                'message' => 'Resource not found'
            ];
            $code = 404;
            return response()->json($body, $code);
        }

        $fields = ['title', 'duration'];
        foreach($fields as $field) {
            if (isset($request->{$field})) {
                $type->{$field} = $request->{$field};
            }
        }

        try {
            $data = $type->save();
            $code = 201;
            $body = $type;
        } catch (\Exception $e) {
            $body = [
                'message' => 'Something went wrong'
            ];
            $code = 400;
        }
        return response()->json($body, $code);
    }
}
