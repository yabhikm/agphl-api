<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Model\EventType;
use App\Model\User;
use App\Model\Schedule;
use DB;

class UserController extends Controller {
    public function __construct() { }

    public function auth (Request $request) {
        return response()->json([
            'id' => $request->user_id,
            'name' => $request->user_name,
            'email' => $request->user_email
        ], 200);
    }

    public function getUserEventTypes(Request $request, $id) {
        $body = [];
        $code = 200;
        try {
            $body = EventType::select('id', 'title', 'duration')->where('user_id', $id)->get();
            return response()->json($body, $code);
        } catch(\Exception $e) {
            return response()->json($body, $code);
        } 
    }

    public function getUserAvailability(Request $request, $id) {        
        $body = [
            'message' => 'Validation Failed'
        ];
        $code = 400;
        $rules = [
            'date' => 'required|regex:/^\d{8}$/',
            'type' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($body, $code);
        }
        $dayStartTime = date('Y-m-d', strtotime($request->date)). ' 10:00:00';
        $dayEndTime = date('Y-m-d', strtotime($request->date)). ' 19:00:00';
        $daySchedule = DB::table('schedule as S')
        ->select('S.id', 'S.from', 'S.to')
        ->join('event_types as E', 'S.type_id', '=', 'E.id')
        ->where('S.from', '>=', $dayStartTime)
        ->where('S.to', '<=', $dayEndTime)
        ->where('E.user_id', $id)
        ->get()->toArray();
        $eventType = EventType::select('duration')->where('id', $request->type)->first();

        $startHour = 10;
        $endHour = 19;
        $intervalBookingTimes = 15;
        $body = [];
        for ($i = $startHour; $i < $endHour; $i++) {
            $h = $i;
            $m = '';
            $period = 'AM';

            if ($i > 12) {
                $period = "PM";
                $h = $h - 12;
            }
            for ($j = 0; $j < 60; $j = $j + $intervalBookingTimes) {
                $m = $j;
                if ($j < 10) {
                    $m = '0' . $j;
                }
                if ($i === 12 && $j > 0) {
                    $period = "PM";
                }
                $st = $h.':'.$m;
                $eh = $h;
                $em = $m + $eventType->duration;
                if ($em >= 60) {
                    $eh = $eh + floor($em / 60);
                    $em = ($em % 60);
                }
                $et = $eh.':'.$em;
                $count = 0;
                foreach($daySchedule as $sch) {
                    $bst = date('H:i:s', strtotime($sch->from));
                    $bet = date('H:i:s', strtotime($sch->to));
                    if (
                        (strtotime($bst) <= strtotime($st) && strtotime($bet) > strtotime($st)) ||
                        (strtotime($bst) <= strtotime($et) && strtotime($bet) > strtotime($et))
                    ) {
                        $count++;
                        break;    
                    }
                }
                if ($count == 0) {
                    $body[] = [
                        'h_12' => $h,
                        'h_24' => $i,
                        'm' => $m,
                        'period' => $period
                    ];
                }
            }
        }
        $code = 200;
        return response()->json($body, $code);
    }

    public function addSchedule(Request $request) {
        $body = [
            'message' => 'Validation Failed'
        ];
        $code = 400;
        $rules = [
            'date' => 'required|digits:14',
            'type' => 'required|integer',
            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required|email'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($body, $code);
        }
        $eventType = EventType::select('duration')->where('id', $request->type)->first();

        $schedule = new Schedule();
        $schedule->type_id = $request->type;
        $schedule->from = date('Y-m-d H:i:s', strtotime($request->date));
        $schedule->to = date('Y-m-d H:i:s', strtotime("+".$eventType->duration." minutes", strtotime($request->date)));
        $schedule->f_name = $request->f_name;
        $schedule->l_name = $request->l_name;
        $schedule->email = $request->email;
        
        try {
            $schedule->save();
            $code = 201;
            $body = $schedule;
        } catch (\Exception $e) {
            $body = [
                'message' => 'Something went wrong'
            ];
        }
        return response()->json($body, $code);
    }

    public function getUser(Request $request, $id) {
        $body = User::select('name', 'id')->where('id', $id)->first();
        return response()->json($body, 200);
    }

    public function getMyUpcomingSchedule(Request $request) {
        $schedule = $this->getMySchedule(true, $request->user_id);
        return response()->json($schedule, 200);
    }

    public function getMyPastSchedule(Request $request) {
        $schedule = $this->getMySchedule(false, $request->user_id);
        return response()->json($schedule, 200);
    }

    public function getMySchedule($upcoming = true, $user_id) {
        $query = DB::table('schedule as S')
        ->select('S.id', 'from', 'f_name', 'l_name', 'E.title')
        ->join('event_types as E', 'S.type_id', '=', 'E.id')
        ->where('E.user_id', $user_id);
        if ($upcoming) {
            $query->where('S.from', '>=', date('Y-m-d'));
        } else {
            $query->where('S.from', '<', date('Y-m-d'));
        }
        return $query->get()->toArray();
    }
}
