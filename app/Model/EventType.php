<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model {
    protected $table = 'event_types';

    protected $fillable = [
        'user_id', 'title', 'duration',
    ];
}
